<?php

require_once "data/Person.php";

$person = new Person();
$person->name = 'Romi';
$person->address = 'Jl. Bumi No. 1';
$person->country = 'Indonesia';

$person2 = new Person();
$person2->name = 'Ibrahim';
$person2->address = 'Jl. Mars No. 1';
$person2->country = 'Indonesia';

var_dump($person);
var_dump($person2);


echo "Nama : $person->name" . PHP_EOL;
echo "Alamat : $person->address" . PHP_EOL;
echo "Negara : $person->country" . PHP_EOL;